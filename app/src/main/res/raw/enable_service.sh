#!/system/bin/sh

# enable accessibility service
SERVICE_NAME=win.limerainne.rididictbridge/.WordLookupService
PREV_SERVICES=$(settings get secure enabled_accessibility_services)

if [[ ${PREV_SERVICES} == *${SERVICE_NAME}* ]]; then
    echo "Service already in list!"
else
    if [ ${PREV_SERVICES} ]; then
        TARGET_SERVICES="${PREV_SERVICES}:${SERVICE_NAME}"
    else
        TARGET_SERVICES="${SERVICE_NAME}"
    fi
    settings put secure enabled_accessibility_services "${TARGET_SERVICES}"
fi

settings put secure accessibility_enabled 1
