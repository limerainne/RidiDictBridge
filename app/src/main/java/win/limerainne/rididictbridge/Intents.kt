package win.limerainne.rididictbridge

object Intents {
    const val INTENT_SETTINGS = "win.limerainne.ridiofflinedictbridge.settings"

    const val ACTION_CHANGE = "change"
    const val ACTION_STEM_WORD = "stemWord"

    const val EXTRA_DICTIONARY_APP = "dictionaryApp"
    const val EXTRA_DO_STEM_WORD = "stemWord"
}