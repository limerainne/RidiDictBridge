package win.limerainne.rididictbridge.dictionary

import android.app.SearchManager
import android.content.Context
import android.content.Intent

/**
 * Created by Limerainne on 2018-04-09.
 */
object WordWebDictHelper: DictHelper {
    val TAG = "DICT_WORDWEB"

    private val WordWebPackageName = "com.wordwebsoftware.android.wordweb"

    private val AndroidProcessTextIntentAction = "android.intent.action.PROCESS_TEXT"
    private val AndroidExtraProcessTextIntent = "android.intent.extra.PROCESS_TEXT"

    override fun query(context: Context, keyword: String) {
        val intent = Intent(Intent.ACTION_SEARCH).let {
            it.`package` = WordWebPackageName
            it.putExtra(SearchManager.QUERY, keyword)
            it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        }
//        val intent = Intent(AndroidProcessTextIntentAction).let {
//            it.`package` = WordWebPackageName
//            it.putExtra(AndroidExtraProcessTextIntent, keyword)
//            it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//        }

        if (isIntentAvailable(context, intent))
            context.startActivity(intent)
    }
}