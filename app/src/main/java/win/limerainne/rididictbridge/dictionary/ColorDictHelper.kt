package win.limerainne.rididictbridge.dictionary

import android.content.Context
import android.content.Intent
import android.graphics.Point
import android.view.Gravity
import android.view.WindowManager

/**
 * Pasted by Limerainne on 2018-01-23.
 *
 * http://blog.socialnmobile.com/2011/01/colordict-api-for-3rd-party-developers.html
 * https://www.androidpub.com/index.php?mid=appnews&search_target=user_id&search_keyword=graynote&document_srl=1241070
 *
 */

object ColorDictHelper: DictHelper {
    val TAG = "DICT_COLORDICT"
    private val SEARCH_ACTION = "colordict.intent.action.SEARCH"

    private val flags = mapOf(
            "q" to "EXTRA_QUERY",

            "w" to "EXTRA_WIDTH",
            "h" to "EXTRA_HEIGHT",
            "g" to "EXTRA_GRAVITY",

            "fs" to "EXTRA_FULLSCREEN",

            "m_l" to "EXTRA_MARGIN_LEFT",
            "m_t" to "EXTRA_MARGIN_TOP",
            "m_b" to "EXTRA_MARGIN_BOTTOM",
            "m_r" to "EXTRA_MARGIN_RIGHT"
    )

    override fun query(context: Context, keyword: String) = openPopup(context, keyword)

    fun openPopup(context: Context, keyword: String) {
        val intent = Intent(SEARCH_ACTION).let {
            it.putExtra(flags["q"], keyword)

            it.putExtra(flags["h"], getWindowHeightPx(context) / 2)
            it.putExtra(flags["g"], Gravity.TOP)

            it.putExtra(flags["fs"], false)

            it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        }

        if (isIntentAvailable(context, intent))
            context.startActivity(intent)
    }

    private fun getWindowHeightPx(context: Context): Int {
        val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = wm.defaultDisplay ?: return 1000

        return Point().let {
            display.getSize(it)
            it.y
        }
    }
}
