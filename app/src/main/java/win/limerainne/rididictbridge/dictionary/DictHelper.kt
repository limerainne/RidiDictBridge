package win.limerainne.rididictbridge.dictionary

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager

/**
 * Created by Limerainne on 2018-04-09.
 */
interface DictHelper {
    fun query(context: Context, keyword: String)

    fun isIntentAvailable(context: Context, intent: Intent): Boolean =
            context.packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY).size > 0
}

fun getDictHelpers(tag: String) =
    when (tag) {
        ColorDictHelper.TAG -> ColorDictHelper
        WordWebDictHelper.TAG -> WordWebDictHelper
        else -> WordWebDictHelper
    }