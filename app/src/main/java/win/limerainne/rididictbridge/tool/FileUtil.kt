package win.limerainne.rididictbridge.tool

import android.content.Context
import java.io.FileOutputStream
import java.io.IOException

// https://stackoverflow.com/questions/19464162/how-to-copy-raw-files-into-sd-card?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa

object FileUtil {
    @Throws(IOException::class)
    fun CopyRAWtoSDCard(context: Context, id: Int, path: String) {
        val src = context.resources.openRawResource(id)
        val dest = FileOutputStream(path)

        try {
            val buff = ByteArray(1024)
            do {
                val read = src.read(buff)
                if (read <= 0)
                    break
                dest.write(buff, 0, read)
            } while (true)

        } finally {
            src.close()
            dest.close()

        }
    }

    @Throws(IOException::class)
    fun CopyRAWtoInternal(context: Context, id: Int, filename: String) {
        val src = context.resources.openRawResource(id)
        val dest = FileOutputStream("${context.filesDir}/${filename}")

        try {
            val buff = ByteArray(1024)
            do {
                val read = src.read(buff)
                if (read <= 0)
                    break
                dest.write(buff, 0, read)
            } while (true)

        } finally {
            src.close()
            dest.close()

        }
    }

}