package win.limerainne.rididictbridge.tool

import android.content.Context
import android.provider.Settings
import android.util.Log


object AccessibilityServiceChecker {
    fun Running(context: Context, serviceName: String): Boolean {
        val enabledServices = Settings.Secure.getString(context.applicationContext.contentResolver, Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES) ?: return false
        val runningServices = enabledServices.split(":".toRegex()).dropLastWhile({ it.isEmpty() })

        val newServiceName = serviceName.replace(context.packageName, context.packageName + "/")

        for (runningService in runningServices) {
            if (runningService.contains(newServiceName)) {
                return true
            }
        }
        return false
    }
}