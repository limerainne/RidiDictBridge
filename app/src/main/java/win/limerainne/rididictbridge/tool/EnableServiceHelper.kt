package win.limerainne.rididictbridge.tool

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Environment
import android.provider.Settings
import android.util.Log
import com.jrummyapps.android.shell.CommandResult
import com.jrummyapps.android.shell.Shell
import win.limerainne.rididictbridge.R

object EnableServiceHelper {

    val methodsArray: Array<Pair<Int, ((Context)->Unit)>> = arrayOf(
            R.string.dialog_enable_service_method_root to { it -> enableWithRootPerm(it) },
            R.string.dialog_enable_service_method_open_settings_screen to { it -> openAccessibilitySettingScreen(it) },
            R.string.dialog_enable_service_method_use_pc to { it -> askUserToRunPCADB(it) }
    )

    fun enableWithRootPerm(context: Context): CommandResult {
        // if root, run script directly
        val scriptFilename = "enable_service.sh"
        FileUtil.CopyRAWtoInternal(context, R.raw.enable_service, scriptFilename)
        val result = Shell.SU.run("sh ${context.filesDir}/$scriptFilename")

        return result
    }

    fun openAccessibilitySettingScreen(context: Context) {
        // if non-root, 1. open accessibility settings screen
        // https://stackoverflow.com/questions/29367093/how-can-i-start-the-accessibility-settings-page-of-my-app-in-android?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
        val intent = Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS)
        context.startActivity(intent)
    }

    fun askUserToRunPCADB(context: Context) {
        // if non-root, 2. copy script then instruct user to run ADB command
        FileUtil.CopyRAWtoSDCard(context, R.raw.enable_service,
                "${Environment.getExternalStorageDirectory()}/enable_service.sh")
        AlertDialog.Builder(context).let {
            it.setTitle(context.getString(R.string.dialog_enable_service_method_pc_next_step_title))
            it.setMessage(context.getString(R.string.dialog_enable_service_method_pc_next_step_msg))

            it.setPositiveButton(android.R.string.ok) {_, _ -> }

            it.create().show()
        }
    }

}