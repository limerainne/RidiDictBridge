package win.limerainne.rididictbridge.view

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.preference.ListPreference
import android.preference.Preference
import android.preference.PreferenceFragment
import android.preference.SwitchPreference
import android.util.Log
import win.limerainne.rididictbridge.Intents
import win.limerainne.rididictbridge.R
import win.limerainne.rididictbridge.WordLookupService
import win.limerainne.rididictbridge.preference.AppPreferences
import win.limerainne.rididictbridge.tool.AccessibilityServiceChecker
import win.limerainne.rididictbridge.tool.EnableServiceHelper

class SettingFragment: PreferenceFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        addPreferencesFromResource(R.xml.preference_app)
    }

    override fun onResume() {
        super.onResume()

        initEnableServicePreference(findPreference(AppPreferences.ENABLE_SERVICE) as SwitchPreference)
        initDictAppPreference(findPreference(AppPreferences.DICTIONARY_APP) as ListPreference)
        initTidyStemWordPreference(findPreference(AppPreferences.TIDY_STEM_WORD) as SwitchPreference)
    }

    private fun initEnableServicePreference(pref: SwitchPreference) {
        with (pref) {
            val isRunning = {
                AccessibilityServiceChecker.Running(context, WordLookupService::class.java.name)
            }

            val updateStatus = {
                val serviceEnabled = isRunning()
                isChecked = serviceEnabled
                isEnabled = !isChecked
                isSelectable = isEnabled

                serviceEnabled
            }
            updateStatus()

            setOnPreferenceChangeListener {
                _, newValue ->
                newValue as Boolean
                val serviceEnabled = updateStatus()

                val applyNewValue: Boolean = !newValue or serviceEnabled

                if (!applyNewValue) {
                    AlertDialog.Builder(activity).let {
                        it.setTitle(getString(R.string.dialog_activate_service_title))
                        it.setItems(EnableServiceHelper.methodsArray.map { getString(it.first) }.toTypedArray()) { _, idx ->
                            EnableServiceHelper.methodsArray[idx].second(context).also {
                                updateStatus()
                            }
                        }

                        it.create().show()
                    }
                }

                applyNewValue
            }
        }
    }

    private fun initDictAppPreference(pref: ListPreference) {
        with (pref) {
            value = activity?.application?.run{ AppPreferences(this).dictionaryApp }
            summary = entry

            onPreferenceChangeListener = Preference.OnPreferenceChangeListener { _, newValue ->
                newValue as String

                activity?.application?.run { AppPreferences(this).dictionaryApp = newValue }
                summary = entries[findIndexOfValue(newValue)]

                val intent = Intent(Intents.ACTION_CHANGE).apply {
                    //                    setClass(context, WordLookupService::class.java)
                    putExtra(Intents.EXTRA_DICTIONARY_APP, newValue)
                }

                try {
                    context.sendBroadcast(intent)
                } catch (e: Exception) {
                    Log.d(TAG, "Service nonexist!")
                }

                true }
        }
    }

    private fun initTidyStemWordPreference(pref: SwitchPreference) {
        with (pref) {
            onPreferenceChangeListener = Preference.OnPreferenceChangeListener { _, newValue ->
                newValue as Boolean

                val intent = Intent(Intents.ACTION_STEM_WORD).apply {
                    putExtra(Intents.EXTRA_DO_STEM_WORD, newValue)
                }

                try {
                    context.sendBroadcast(intent)
                } catch (e: Exception) {
                    Log.d(TAG, "Service nonexist!")
                }

                true
            }
        }
    }

    companion object {
        const val TAG = "SETTING"
    }
}