package win.limerainne.rididictbridge.view

import android.content.Context
import android.graphics.PixelFormat
import android.os.Build
import android.view.Gravity
import android.view.LayoutInflater
import android.view.WindowManager
import android.widget.FrameLayout
import win.limerainne.rididictbridge.R

// refer to..
// https://codelabs.developers.google.com/codelabs/developing-android-a11y-service/index.html?index=..%2F..%2Findex#3
// http://www.hardcopyworld.com/ngine/android/index.php/archives/162

open class FloatingButton {
    protected lateinit var mLayout: FrameLayout
    protected lateinit var mLP: WindowManager.LayoutParams
    fun createView(context: Context) {
        mLayout = FrameLayout(context)
        mLP = WindowManager.LayoutParams().apply {
            type = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1)
                        WindowManager.LayoutParams.TYPE_ACCESSIBILITY_OVERLAY
                    else
//                        WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY
                        WindowManager.LayoutParams.TYPE_PHONE

            format = PixelFormat.TRANSLUCENT

            flags = flags or WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE

            width = WindowManager.LayoutParams.WRAP_CONTENT
            height = WindowManager.LayoutParams.WRAP_CONTENT
            gravity = Gravity.TOP
        }

        LayoutInflater.from(context).apply {
            inflate(R.layout.floating_button, mLayout)
        }
    }

    fun attachView(context: Context) {
        (context.getSystemService(Context.WINDOW_SERVICE) as WindowManager).apply {
            addView(mLayout, mLP)
        }
    }

    fun detachView(context: Context) {
        try {
            (context.getSystemService(Context.WINDOW_SERVICE) as WindowManager).apply {
                removeView(mLayout)
            }
        } catch (e: IllegalArgumentException) {}
    }
}