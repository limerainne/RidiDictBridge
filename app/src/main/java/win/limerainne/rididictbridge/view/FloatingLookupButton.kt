package win.limerainne.rididictbridge.view

import android.content.Context
import android.view.View
import android.widget.Button
import win.limerainne.rididictbridge.R

class FloatingLookupButton : FloatingButton() {

    fun setLookupBehavior(context: Context, word: String, callback: () -> Unit) {
        mLayout.findViewById<Button>(R.id.lookup)?.apply {
            mLayout.visibility = View.VISIBLE
            this.text = context.getString(R.string.lookup_with_word, word)

            setOnClickListener {
                mLayout.visibility = View.INVISIBLE
                callback()
            }
        }
    }
}