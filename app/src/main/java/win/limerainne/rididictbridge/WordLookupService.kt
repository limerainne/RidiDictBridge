package win.limerainne.rididictbridge

import android.accessibilityservice.AccessibilityService
import android.content.*
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.os.Build
import android.util.Log
import android.view.accessibility.AccessibilityEvent
import android.view.accessibility.AccessibilityNodeInfo
import win.limerainne.rididictbridge.accessibility.ExploreView
import win.limerainne.rididictbridge.dictionary.WordWebDictHelper
import win.limerainne.rididictbridge.dictionary.getDictHelpers
import win.limerainne.rididictbridge.preference.AppPreferences
import win.limerainne.rididictbridge.view.FloatingLookupButton


// http://pluu.github.io/blog/android/droidkaigi/2017/10/20/droidkaigi-2017-AccessibilityService/


class WordLookupService : AccessibilityService() {

    private var word = ""
    private lateinit var floatingLookupButton: FloatingLookupButton

    private var dictionaryAppTag = WordWebDictHelper.TAG
    private var tidyStemWord = true

    private val intentReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            when (intent?.action) {
                Intents.ACTION_CHANGE -> {
                    when {
                        intent.hasExtra(Intents.EXTRA_DICTIONARY_APP) -> {
                            dictionaryAppTag = intent.getStringExtra(Intents.EXTRA_DICTIONARY_APP)
                        }
                    }
                }
                Intents.ACTION_STEM_WORD -> {
                    when {
                        intent.hasExtra(Intents.EXTRA_DO_STEM_WORD) -> {
                            tidyStemWord = intent.getBooleanExtra(Intents.EXTRA_DO_STEM_WORD, true)
                        }
                    }
                }
            }
        }
    }

    override fun onServiceConnected() {
        super.onServiceConnected()

        floatingLookupButton = FloatingLookupButton().apply { createView(applicationContext) }

        dictionaryAppTag = AppPreferences(application).dictionaryApp
        tidyStemWord = AppPreferences(application).tidyStemWord

        registerReceiver(intentReceiver, IntentFilter(Intents.ACTION_CHANGE))
    }

    override fun onInterrupt() {}

    override fun onDestroy() {
        super.onDestroy()

        unregisterReceiver(intentReceiver)
    }

    override fun onAccessibilityEvent(event: AccessibilityEvent) {
        putDebugMessage(event)

        when (event.eventType) {
            AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED -> onWindowStateChanged(event)
        }
    }

    private fun putDebugMessage(event: AccessibilityEvent) {

    }

    private fun onWindowStateChanged(event: AccessibilityEvent) {
        if (event.packageName != null && event.className != null) {
            val componentName = ComponentName(
                    event.packageName.toString(),
                    event.className.toString()
            )

            onActivityChanged(event, componentName)
        }
    }

    private fun tryGetActivity(componentName: ComponentName): ActivityInfo? = try {
        packageManager.getActivityInfo(componentName, 0)
    } catch (e: PackageManager.NameNotFoundException) {
        null
    }

    private fun onActivityChanged(event: AccessibilityEvent, componentName: ComponentName) {
        when {
            isRidiMemoActivity(componentName) -> pickWordThenShowButton({
                pickWordFromRidiMemoActivity(event)
            }, {
                val source: AccessibilityNodeInfo = ExploreView.pickRootViewNode(this) ?: throw ExploreView.NoSuitableViewException()

                ExploreView.exploreViewHierarchy(source, {
                    val child = it
                    val className = child.className?.toString() ?: ""

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2)
                        child.viewIdResourceName?.endsWith("cancel_button") ?: false
                    else
                        className.endsWith("Button") and (child.text?.contains("취소") ?: false)
                }, {it})
            })
            isRidiSearchActivity(componentName) -> pickWordThenShowButton({
                pickWordFromRidiSearchActivity(event)
            }, {
                val source: AccessibilityNodeInfo = ExploreView.pickRootViewNode(this) ?: throw ExploreView.NoSuitableViewException()

                ExploreView.exploreViewHierarchy(source, {
                    val child = it
                    val className = child.className?.toString() ?: ""

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2)
                        child.viewIdResourceName?.endsWith("close") ?: false
                    else
                        className.endsWith("TextView") and (child.text?.contains("닫기") ?: false)
                }, {it})
            })
            isRidiContextTooltip(componentName) -> {
                showDictButton(event)
            }
            else -> floatingLookupButton.detachView(applicationContext)
        }
    }

    private fun pickWordThenShowButton(pickWord: () -> String, getCloseButton: () -> AccessibilityNodeInfo) {
        try {
            // Log.i("WordLookup", "Finding words...")
            pickWord().let {
                // Log.i("WordLookup", it)

                this.word = it
                showLookupButton(getCloseButton())
            }
        } catch (e: ExploreView.NoSuitableViewException) {
            Log.d("WordLookup", "No words detected!")
            e.printStackTrace()
        }
    }

    private fun showLookupButton(closeButton: AccessibilityNodeInfo) {
        floatingLookupButton.apply {
            setLookupBehavior(applicationContext, word) {
//                if ((applicationContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).isAcceptingText)
//                    performGlobalAction(GLOBAL_ACTION_BACK)
//
//                performGlobalAction(GLOBAL_ACTION_BACK) // close memo activity

                closeButton.performAction(AccessibilityNodeInfo.ACTION_CLICK)

                getDictHelpers(dictionaryAppTag).query(applicationContext, word)
            }
            attachView(applicationContext)
        }
    }

    private fun showDictButton(event: AccessibilityEvent) {

    }

    private fun isRidiMemoActivity(componentName: ComponentName): Boolean {
        val nameRidiApp = "com.initialcoms.ridi"
        val nameRidiPaperApp = "com.ridi.paper"
        val nameRidiMemoActivity = "MemoEditorActivity"

        return (componentName.packageName == nameRidiPaperApp || componentName.packageName == nameRidiApp)
                && componentName.className.endsWith(nameRidiMemoActivity)
    }

    private fun isRidiSearchActivity(componentName: ComponentName): Boolean {
        val nameRidiApp = "com.initialcoms.ridi"
        val nameRidiPaperApp = "com.ridi.paper"
        val nameRidiSearchActivity = "TextSearchResultActivity"

        return (componentName.packageName == nameRidiPaperApp || componentName.packageName == nameRidiApp)
                && componentName.className.endsWith(nameRidiSearchActivity)
    }

    private fun isRidiContextTooltip(componentName: ComponentName): Boolean {
        val nameRidiApp = "com.initialcoms.ridi"
        val nameRidiContextTooltipLayout = "android.widget.FrameLayout"

        return componentName.packageName == nameRidiApp
                && componentName.className.endsWith(nameRidiContextTooltipLayout)
    }

    @Throws(ExploreView.NoSuitableViewException::class)
    private fun pickWordFromRidiMemoActivity(event: AccessibilityEvent): String {
        val source: AccessibilityNodeInfo = ExploreView.pickRootViewNode(this) ?: throw ExploreView.NoSuitableViewException()

        fun isTarget(child: AccessibilityNodeInfo): Boolean {
            val className = child.className?.toString() ?: ""
            if (className.endsWith("TextView")) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                    if (child.viewIdResourceName?.endsWith("selected_text") ?: false) {
                        val text = child.text
                        if (text.split(' ').size == 1)
                            return true
                    }
                }

                val text = child.text
                if (text != null && !text.startsWith("메모"))
                    if (text.split(' ').size == 1)
                        return true
            }

            return false
        }

        return ExploreView.exploreViewHierarchy(source, {isTarget(it)}, {tidyWord(it.text.toString())})
    }

    @Throws(ExploreView.NoSuitableViewException::class)
    private fun pickWordFromRidiSearchActivity(event: AccessibilityEvent): String {
        val source: AccessibilityNodeInfo = ExploreView.pickRootViewNode(this) ?: throw ExploreView.NoSuitableViewException()

        fun isTarget(child: AccessibilityNodeInfo): Boolean {
            val className = child.className?.toString() ?: ""
            if (className.endsWith("EditText")) {

                val text = child.text
                if (text != null)
                    if (text.split(' ').size == 1)
                        return true
            }

            return false
        }

        return ExploreView.exploreViewHierarchy(source, {isTarget(it)}, {tidyWord(it.text.toString())})
    }

    private fun tidyWord(word: String): String {
        val wordNoPunctuation = word.replace(Regex("[.,:;!?'`\"()<>{}/”~’“‘´]"), "")
        var wordNormalForm: String = wordNoPunctuation

        if (tidyStemWord) {
            val extendForms = listOf("ing", "ed", "es", "s")
            for (extend in extendForms) {
                if (wordNormalForm.endsWith(extend))
                    wordNormalForm = wordNormalForm.dropLast(extend.length)
            }
        }

        return wordNormalForm
    }

}
