package win.limerainne.rididictbridge

import android.app.Activity
import android.os.Bundle
import win.limerainne.rididictbridge.view.SettingFragment

// reference
// https://jungwoon.github.io/android/2016/10/03/Accessibility-Service/
// https://stackoverflow.com/questions/3873659/android-how-can-i-get-the-current-foreground-activity-from-a-service/27642535#27642535

class MainActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        fragmentManager.beginTransaction()
                .add(R.id.placeholder, SettingFragment(), SettingFragment.TAG)
                .commit()
        fragmentManager.executePendingTransactions()
    }

}
