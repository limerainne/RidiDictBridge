package win.limerainne.rididictbridge.preference

import android.app.Application
import android.content.Context
import win.limerainne.rididictbridge.Intents
import win.limerainne.rididictbridge.dictionary.WordWebDictHelper

class AppPreferences(application: Application) {
    private val preferences = application.getSharedPreferences("settings", Context.MODE_PRIVATE)

    var dictionaryApp by StringPreference(DICTIONARY_APP, WordWebDictHelper.TAG, preferences).delegate()
    var tidyStemWord by BooleanPreference(TIDY_STEM_WORD, true, preferences).delegate()

    companion object {
        const val ENABLE_SERVICE = "enableService"
        const val DICTIONARY_APP = Intents.EXTRA_DICTIONARY_APP
        const val TIDY_STEM_WORD = Intents.EXTRA_DO_STEM_WORD
    }
}
