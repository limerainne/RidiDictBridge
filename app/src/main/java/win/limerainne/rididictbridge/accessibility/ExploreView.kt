package win.limerainne.rididictbridge.accessibility

import android.accessibilityservice.AccessibilityService
import android.view.accessibility.AccessibilityNodeInfo
import java.util.*

object ExploreView {

    fun <T> exploreViewHierarchy(root: AccessibilityNodeInfo,
                                         isTarget: (AccessibilityNodeInfo) -> Boolean,
                                         getTarget: (AccessibilityNodeInfo) -> T): T {
        val queue = ArrayDeque<AccessibilityNodeInfo>()
        queue.push(root)

        while (queue.isNotEmpty()) {
            val child = queue.poll()

            if (isTarget(child)) {
                return getTarget(child)
            }

            for (i in 0 until child.childCount)
                queue.push(child.getChild(i) ?: continue)
        }

        throw NoSuitableViewException()
    }

    fun pickRootViewNode(service: AccessibilityService): AccessibilityNodeInfo? {
        val maxTrial = 25
        val sleepPeriod: Long = 20

        var trial = 0
        var sourceTry: AccessibilityNodeInfo? = null

        while (trial < maxTrial) {
            sourceTry = service.rootInActiveWindow
            if (sourceTry != null)
                break
            trial++
            Thread.sleep(sleepPeriod)
        }

        return sourceTry
    }

    class NoSuitableViewException: Exception()
}